
from abc import ABCMeta, abstractmethod


class Sender(metaclass=ABCMeta):
    """."""
    def __init__(self, address, weight):
        """."""
        self._address = address
        self._weight = weight

    @property
    def address(self):
        """"."""
        return self._address

    @address.setter
    def address(self, address):
        """."""
        self._adress = address

    @property
    def weight(self):
        """."""
        return self._weight

    @weight.setter
    def weight(self, weight):
        """."""
        self._weight = weight

    @abstractmethod
    def getCost(self, weight):
        """."""
        pass

class Letter(Sender):
    """."""
    def __init__(self, address, weight):
        """"."""
        super().__init__(address, weight)

    def getCost(self, weight_letter):
        """."""
        cost_weights = [(0.5, 20), (0.75, 50), (1, 100), (2, 150), (2.5, 200)]
        for cost, weight in cost_weights:
            if weight_letter < weight:
                return cost
        return None

    def __str__(self):
        """."""
        return "Destination letter: {}\nweight: {}".format(self.address, self.weight)

if __name__ == '__main__':
    letter = Letter("Rue Carnot", 50)
    print(letter)